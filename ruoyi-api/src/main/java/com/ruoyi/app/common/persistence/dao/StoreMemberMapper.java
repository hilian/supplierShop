package com.ruoyi.app.common.persistence.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.app.common.persistence.model.StoreMember;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface StoreMemberMapper extends BaseMapper<StoreMember> {
	
}
